﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using Terraria.ObjectData;
using Terraria.DataStructures;
using Terraria.Enums;

namespace AlloysAplenty.Tiles
{
    public class BeigeTorchTile : ModTile
    {
        public override void SetDefaults()
        {
            //Main.tileFlame[Type] = true;
            Main.tileLighted[Type] = true;
            Main.tileFrameImportant[Type] = true;
            Main.tileSolid[Type] = false;
            Main.tileNoAttach[Type] = true;
            Main.tileNoFail[Type] = true;
            Main.tileWaterDeath[Type] = true;

            //TileObjectData.newTile.CopyFrom(TileObjectData.StyleTorch);
            //TileObjectData.newTile.CoordinateHeights = new int[] { 20 };
            //TileObjectData.addTile(Type);


            TileObjectData.newTile.CopyFrom(TileObjectData.StyleTorch);
            TileObjectData.newTile.AnchorBottom = new AnchorData(AnchorType.SolidTile | AnchorType.SolidSide, TileObjectData.newTile.Width, 0); //the default torch orientation is on the ground
            //TileObjectData.newTile.StyleWrapLimit = 1;
            //TileObjectData.newTile.StyleMultiplier = 1;
            TileObjectData.newAlternate.CopyFrom(TileObjectData.StyleTorch);
            TileObjectData.newAlternate.AnchorLeft = new AnchorData(AnchorType.SolidTile | AnchorType.SolidSide | AnchorType.Tree | AnchorType.AlternateTile, TileObjectData.newTile.Height, 0); //torch with wall on left
            TileObjectData.newAlternate.AnchorAlternateTiles = new int[]
            {
                124 //allows placement on wooden beams
            };
            TileObjectData.newTile.CoordinateHeights = new int[] { 20 };
            TileObjectData.addAlternate(1);
            TileObjectData.newAlternate.CopyFrom(TileObjectData.StyleTorch);
            TileObjectData.newAlternate.AnchorRight = new AnchorData(AnchorType.SolidTile | AnchorType.SolidSide | AnchorType.Tree | AnchorType.AlternateTile, TileObjectData.newTile.Height, 0); //torch with wall on right
            TileObjectData.newAlternate.AnchorAlternateTiles = new int[]
            {
                124 //allows placement on wooden beams
            };
            TileObjectData.newTile.CoordinateHeights = new int[] { 40 };
            TileObjectData.addAlternate(2);
            TileObjectData.newAlternate.CopyFrom(TileObjectData.StyleTorch);
            TileObjectData.newAlternate.AnchorWall = true; //torch on wall
            TileObjectData.newTile.CoordinateHeights = new int[] { 60 };
            TileObjectData.addAlternate(0);

            drop = mod.ItemType("BeigeTorch");
            soundStyle = 1;
            mineResist = 0.1f;
            dustType = mod.DustType("QuartzBoltDust");
            AddMapEntry(new Color((int)(255 * 0.7255f), (int)(255 * 0.7020f), (int)(255 * 0.6667f)));
            AddToArray(ref TileID.Sets.RoomNeeds.CountsAsTorch);
            adjTiles = new int[]{TileID.Torches};
        }
    }
}
