﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;


namespace AlloysAplenty.Tiles
{
    public class FoolsPlatinumBarTile : ModTile
    {
        public override void SetDefaults()
        {
            Main.tileShine[Type] = 1100;
            Main.tileSolid[Type] = true;
            Main.tileSolidTop[Type] = true;
            Main.tileFrameImportant[Type] = true;
            drop = mod.ItemType("FoolsPlatinumBar");
            soundStyle = 1;
            mineResist = 0.1f;
            AddMapEntry(new Color(182, 191, 206), "Fool's Platinum Bar");
        }
    }
}
