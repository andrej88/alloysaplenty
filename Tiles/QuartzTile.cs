﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.ObjectData;


namespace AlloysAplenty.Tiles
{
    public class QuartzTile : ModTile
    {
        public override void SetDefaults()
        {
            Main.tileSpelunker[Type] = true;
            Main.tileFrameImportant[Type] = true;
            Main.tileShine[Type] = 500;
            Main.tileShine2[Type] = true;
            Main.tileObsidianKill[178] = true;
            Main.tileNoAttach[Type] = true;

            mineResist = 1f;
            drop = mod.ItemType("Quartz");
            soundStyle = 1;
            AddMapEntry(new Color(204, 199, 192), "Quartz");
        }
    }
}