﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;


namespace AlloysAplenty.Tiles
{
    public class BronzeBarTile : ModTile
    {

        public override void SetDefaults()
        {
            Main.tileShine[Type] = 1100;
            Main.tileSolid[Type] = true;
            Main.tileSolidTop[Type] = true;
            Main.tileFrameImportant[Type] = true;
            drop = mod.ItemType("BronzeBar");
            soundStyle = 1;
            mineResist = 0.1f;
            AddMapEntry(new Color(99, 78, 57), "Bronze Bar");
        }

        /*public override bool CanPlace(int i, int j)
        {
            if (Main.tile[i, j + 1].type == 0)
            {
                return false;
            }
            return false;
        }*/

    }
}
