﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;


namespace AlloysAplenty.Tiles
{
    public class ElectrumBarTile : ModTile
    {
        public override void SetDefaults()
        {
            Main.tileShine[Type] = 1100;
            Main.tileSolid[Type] = true;
            Main.tileSolidTop[Type] = true;
            Main.tileFrameImportant[Type] = true;
            drop = mod.ItemType("ElectrumBar");
            soundStyle = 1;
            mineResist = 0.1f;
            AddMapEntry(new Color(205, 206, 179), "Electrum Bar");
        }
    }
}
