﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.NPCs
{
    public class QuartzDrop : GlobalNPC
    {
        public override void NPCLoot(NPC npc)
        {
            Random rand = new Random();
            if (npc.type == NPCID.Antlion || npc.type == NPCID.WalkingAntlion || npc.type == NPCID.FlyingAntlion)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Quartz"), rand.Next(0, 2));
            }
        }
    }
}