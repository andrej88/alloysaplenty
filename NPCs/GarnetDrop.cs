﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.NPCs
{
    public class GarnetDrop : GlobalNPC
    {
        public override void NPCLoot(NPC npc)
        {
            Random rand = new Random();
            if (npc.type == NPCID.Medusa)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Garnet"), rand.Next(8, 14));
            }
            else if (npc.type == NPCID.GreekSkeleton)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Garnet"), rand.Next(1, 3));
            }
        }
    }
}