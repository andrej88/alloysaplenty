﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.NPCs
{
    public class OpalDrop : GlobalNPC
    {
        public override void NPCLoot(NPC npc)
        {
            Random rand = new Random();
            if (npc.type == NPCID.MushiLadybug || npc.type == NPCID.ZombieMushroom || npc.type == NPCID.ZombieMushroomHat || npc.type == NPCID.FungiBulb || npc.type == NPCID.AnomuraFungus)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Opal"), rand.Next(0, 2));
            }
        }
    }
}