﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.NPCs
{
    public class PeridotDrop : GlobalNPC
    {
        public override void NPCLoot(NPC npc)
        {
            Random rand = new Random();
            if (npc.type == NPCID.GraniteFlyer)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Peridot"), rand.Next(0, 2));
            }
            else if (npc.type == NPCID.GraniteGolem)
            {
                Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, mod.ItemType("Peridot"), rand.Next(5, 8));
            }
        }
    }
}