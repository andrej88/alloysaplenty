﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace AlloysAplenty.Projectiles
{
    public class GarnetBolt : ModProjectile
    {
        public override void SetDefaults()
        {
            projectile.name = "Garnet Bolt";
            projectile.width = 10;
            projectile.height = 10;
            projectile.aiStyle = 29;
            projectile.alpha = 255;
            projectile.magic = true;
            projectile.friendly = true;

            projectile.penetrate = 1;
        }

        public override void AI()
        {
            //ModDust.NewDust(projectile.position + projectile.velocity, projectile.width, projectile.height, mod, "GarnetBoltDust", projectile.velocity.X * 0.5f, projectile.velocity.Y * 0.5f);
            //Dust.NewDust(projectile.position + new Vector2(projectile.width * 0.5f, projectile.height * 0.5f) + projectile.velocity,
            //    projectile.width / 3, 
            //    projectile.height / 3, 
            //    mod, 
            //    "GarnetBoltDust", 
            //    projectile.velocity.X * 0.1f, 
            //    projectile.velocity.Y * 0.01f);
            Dust.NewDust(projectile.position + new Vector2(projectile.width * 0.5f, projectile.height * 0.5f) + projectile.velocity,
                projectile.width / 3,
                projectile.height / 3,
                mod.DustType("GarnetBoltDust"));
            Dust.NewDust(projectile.position + new Vector2(projectile.width * 0.5f, projectile.height * 0.5f) + projectile.velocity,
                projectile.width / 3,
                projectile.height / 3,
                mod.DustType("GarnetBoltDust"));
            //Dust.NewDust(projectile.position + new Vector2(projectile.width * 0.5f, projectile.height * 0.5f) + projectile.velocity, projectile.width / 3, projectile.height / 3, mod, "GarnetBoltDust", projectile.velocity.X * 0.1f, projectile.velocity.Y * 0.01f);
        }

        public override void Kill(int timeLeft)
        {
            for (int k = 0; k < 5; k++)
            {
                Dust.NewDust(projectile.position + projectile.velocity, projectile.width, projectile.height, mod.DustType("GarnetBoltDust"), projectile.oldVelocity.X * 2.0f, projectile.oldVelocity.Y * 2.0f);
            }
            Main.PlaySound(0, (int)projectile.position.X, (int)projectile.position.Y, 3);
        }

    }
}