using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;


namespace AlloysAplenty {
public class AlloysAplenty : Mod
{
    public override void SetModInfo(out string name, ref ModProperties properties)
    {
        name = "AlloysAplenty";
        properties.Autoload = true;
        properties.AutoloadGores = true;
        properties.AutoloadSounds = true;
    }

    public override void Load()
    {
        
    }

    public override void AddCraftGroups()
    {
        AddCraftGroup("AnyGoldenBar", Lang.misc[37] + " Golden Bar", ItemType("RoseGoldBar"), ItemID.GoldBar);
        AddCraftGroup("AnyPlatinumBar", Lang.misc[37] + " Platinum Bar", ItemType("FoolsPlatinumBar"), ItemID.PlatinumBar);
        AddCraftGroup("AnyRedGem", Lang.misc[37] + " Red Gem", ItemType("Garnet"), ItemID.Ruby);
        AddCraftGroup("AnyGreenGem", Lang.misc[37] + " Green Gem", ItemType("Peridot"), ItemID.Emerald);
    }

    public override void AddRecipes()
    {
        ModifyVanillaRecipes();
        RemoveVanillaRecipes();
        StainedGlassRecipes();

        string[] alloys = new string[]
        {
            "Bronze",
            "Electrum",
            "Hepatizon",
            "Tungsteel"
        };

         foreach (string s in alloys)
         {
             AddPickaxe(s);
             AddAxe(s);
             AddHammer(s);
             AddBroadsword(s);
             AddBow(s);
             AddStaff(s);
             AddHelmet(s);
             AddChainmail(s);
             AddGreaves(s);
         }

        //CheatyDevRecipes();
    }

    private void AddStaff(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        recipe.AddTile(TileID.Anvils);
        if (s == "Bronze")
        {
            recipe.AddIngredient(this, s + "Bar", 8);
            recipe.AddIngredient(this, "Quartz", 8);
            recipe.SetResult(this, "QuartzStaff");
        }
        else
        {
            recipe.AddIngredient(this, s + "Bar", 10);
            if (s == "Electrum")
            {
                recipe.AddIngredient(this, "Garnet", 8);
                recipe.SetResult(this, "GarnetStaff");
            }
            else if (s == "Hepatizon")
            {
                recipe.AddIngredient(this, "Peridot", 8);
                recipe.SetResult(this, "PeridotStaff");
            }
            else if (s == "Tungsteel")
            {
                recipe.AddIngredient(this, "Opal", 8);
                recipe.SetResult(this, "OpalStaff");
            }
        }
        recipe.AddRecipe();
    }

    private void AddBow(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 5);
        else
            recipe.AddIngredient(this, s + "Bar", 7);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Bow");
        recipe.AddRecipe();
    }

    private void AddBroadsword(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 6);
        else
            recipe.AddIngredient(this, s + "Bar", 8);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Broadsword");
        recipe.AddRecipe();
    }

    private void AddGreaves(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 20);
        else
            recipe.AddIngredient(this, s + "Bar", 25);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Greaves");
        recipe.AddRecipe();
    }

    private void AddChainmail(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 25);
        else
            recipe.AddIngredient(this, s + "Bar", 30);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Chainmail");
        recipe.AddRecipe();
    }

    private void AddHelmet(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 15);
        else
            recipe.AddIngredient(this, s + "Bar", 20);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Helmet");
        recipe.AddRecipe();
    }

    private void AddHammer(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 8);
        else
            recipe.AddIngredient(this, s + "Bar", 10);
        recipe.AddCraftGroup(CraftGroup.GetVanillaGroup("Wood"), 3);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Hammer");
        recipe.AddRecipe();
    }

    private void AddAxe(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 7);
        else
            recipe.AddIngredient(this, s + "Bar", 9);
        recipe.AddCraftGroup(CraftGroup.GetVanillaGroup("Wood"), 3);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Axe");
        recipe.AddRecipe();
    }

    private void AddPickaxe(string s)
    {
        ModRecipe recipe;
        recipe = new ModRecipe(this);
        if (s == "Bronze")
            recipe.AddIngredient(this, s + "Bar", 10);
        else
            recipe.AddIngredient(this, s + "Bar", 12);
        recipe.AddCraftGroup(CraftGroup.GetVanillaGroup("Wood"), 3);
        recipe.AddTile(TileID.Anvils);
        recipe.SetResult(this, s + "Pickaxe");
        recipe.AddRecipe();
    }

    private void StainedGlassRecipes()
    {
        ModRecipe recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.GlassWall, 20);
        recipe.AddCraftGroup(this, "AnyRedGem", 1);
        recipe.SetResult(ItemID.RedStainedGlass, 20);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.GlassWall, 20);
        recipe.AddCraftGroup(this, "AnyGreenGem", 1);
        recipe.SetResult(ItemID.GreenStainedGlass, 20);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.GlassWall, 20);
        recipe.AddIngredient(this, "Opal", 1);
        recipe.SetResult(ItemID.MulticoloredStainedGlass, 20);
        recipe.AddRecipe();
    }

    private void RemoveVanillaRecipes()
    {
        int[] items = new int[]{
            ItemID.Throne,
            ItemID.GoldChandelier,
            ItemID.Candelabra,
            ItemID.Candle,
            ItemID.GoldCrown,
            ItemID.GoldWatch,
            ItemID.Throne,
            ItemID.PlatinumChandelier,
            ItemID.PlatinumCandelabra,
            ItemID.PlatinumCandle,
            ItemID.PlatinumCrown,
            ItemID.PlatinumWatch,
            ItemID.RedStainedGlass,
            ItemID.GreenStainedGlass
        };

        ModRecipe recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.RedPotion);
        recipe.SetResult(ItemID.RedPotion);
        recipe.AddRecipe();

        foreach (int i in items)
        {
            bool removedRecipe = false;
            for (int j = 0; j < Recipe.numRecipes; j++)
            {
                if (!removedRecipe && Main.recipe[j].createItem.type == i)
                {
                    removedRecipe = true;
                }
                if (removedRecipe)  // if you have removed the old recipe, then move each subsequent recipe back one spot
                {
                    if (j < Recipe.numRecipes - 1)
                    {
                        Main.recipe[j] = Main.recipe[j + 1];
                    }
                }
            }
        }
    }

    private void CheatyDevRecipes()
    {
        ModRecipe recipe;

        int[] vanillaMaterials = new int[]{
            ItemID.CopperBar,
            ItemID.TinBar,
            ItemID.IronBar,
            ItemID.LeadBar,
            ItemID.SilverBar,
            ItemID.TungstenBar,
            ItemID.GoldBar,
            ItemID.PlatinumBar,
            ItemID.MeteoriteBar,
            ItemID.Amethyst,
            ItemID.Topaz,
            ItemID.Sapphire,
            ItemID.Emerald,
            ItemID.Ruby,
            ItemID.Diamond,
            ItemID.CrystalShard,
            ItemID.Silk
        };

        foreach (int i in vanillaMaterials)
        {
            recipe = new ModRecipe(this);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(i, 50);
            recipe.AddRecipe();
        }


        string[] modMaterials = new string[]{
            "BronzeBar",
            "ElectrumBar",
            "HepatizonBar",
            "TungsteelBar",
            "Quartz",
            "Garnet",
            "Peridot",
            "Opal"
        };

        foreach (string i in modMaterials)
        {
            recipe = new ModRecipe(this);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(this, i, 99);
            recipe.AddRecipe();
        }

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.EndlessQuiver);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.TargetDummy);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.Sundial);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.MythrilAnvil);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.WizardHat);
        recipe.AddRecipe();

        recipe = new ModRecipe(this);
        recipe.AddIngredient(ItemID.DirtBlock);
        recipe.SetResult(ItemID.MagicHat);
        recipe.AddRecipe();

    }

    private void ModifyVanillaRecipes()
    {
        ModifyMetalBarRequirements();
    }

    private void ModifyMetalBarRequirements()
    {
        Main.recipe[1137].requiredItem[0].stack = 3; // copper bar
        Main.recipe[1150].requiredItem[0].stack = 3; // tin bar
        Main.recipe[1163].requiredItem[0].stack = 3; // iron bar
        Main.recipe[1175].requiredItem[0].stack = 3; // lead bar
        Main.recipe[1197].requiredItem[0].stack = 3; // silver bar
        Main.recipe[1210].requiredItem[0].stack = 3; // tungsten bar
        Main.recipe[1223].requiredItem[0].stack = 3; // gold bar
        Main.recipe[1240].requiredItem[0].stack = 3; // platinum bar
    }
    
}}