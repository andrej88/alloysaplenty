﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace AlloysAplenty.Dusts
{
    public class GarnetBoltDust : ModDust
    {
        public override void OnSpawn(Dust dust)
        {
            dust.velocity.Y *= 0.1f;
            dust.velocity.X *= 0.1f;
            dust.noGravity = true;
            dust.noLight = true;
            dust.scale *= 1.5f;
            dust.alpha = 100;
        }

        public override bool Update(Dust dust)
        {
            dust.position += dust.velocity;
            dust.rotation += dust.velocity.X * 0.15f;
            dust.scale *= 0.91f;
            float light = 0.35f * dust.scale;
            Lighting.AddLight(dust.position, light * 0.886f, light * 0.388f, light * 0.518f);
            if (dust.scale < 0.2f)
            {
                dust.active = false;
            }
            return false;
        }
    }
}