﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class TungsteelBow : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Tungsteel Bow"; //
            item.useStyle = 5;
            item.useAnimation = 30;     //
            item.useTime = 30;          //
            item.width = 16;
            item.height = 32;
            item.shoot = 1;
            item.useAmmo = ProjectileID.WoodenArrowFriendly;
            item.useSound = 5;
            item.damage = 20;            //
            item.shootSpeed = 9f;     //
            item.noMelee = true;
            item.value = Item.sellPrice(0, 0, 6, 60);
            item.ranged = true;
            item.rare = 0;              //
            item.autoReuse = false;      //
        }
    }
}