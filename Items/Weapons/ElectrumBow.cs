﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class ElectrumBow : ModItem
    {
        private int projectileSpread = 15;
        public override void SetDefaults()
        {
            item.name = "Electrum Bow"; //
            item.useStyle = 5;
            item.useAnimation = 15;     //
            item.useTime = 15;          //
            item.width = 16;
            item.height = 32;
            item.shoot = 1;
            item.useAmmo = ProjectileID.WoodenArrowFriendly;
            item.useSound = 5;
            item.damage = 1;            //
            item.shootSpeed = 8f;     //
            item.noMelee = true;
            item.value = Item.sellPrice(0, 0, 10, 50);
            item.ranged = true;
            item.rare = 0;              //
            item.autoReuse = true;      //

        }

        public override bool Shoot(Player player, ref Microsoft.Xna.Framework.Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            float newAngleOffset = (float)((Main.rand.Next(projectileSpread) - (projectileSpread / 2)) * (3.14159 / 180));
            float currentAngle;
            if (speedX > 0)
            {
                currentAngle = (float)Math.Atan(speedY / speedX);
            }
            else
            {
                currentAngle = (float)Math.Atan(speedY / speedX) + 3.14159f;
            }
            float newAngle = currentAngle + newAngleOffset;
            speedX = item.shootSpeed * (float)Math.Cos((double)newAngle);
            speedY = item.shootSpeed * (float)Math.Sin((double)newAngle);
            return true;
        }
    }
}