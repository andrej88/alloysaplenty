﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class QuartzStaff : ModItem
    {

        public override void SetDefaults()
        {
            item.name = "Quartz Staff";
            item.useSound = 43;
            item.useStyle = 5;
            item.width = 40;
            item.height = 40;
            item.magic = true;
            item.noMelee = true;
            Item.staff[item.type] = true;

            item.mana = 5;
            item.damage = 15;
            item.shoot = mod.ProjectileType("QuartzBolt");
            item.useAnimation = 38;
            item.useTime = 38;
            item.shootSpeed = 6.5f;
            item.knockBack = 3.5f;
            item.autoReuse = false;
            item.value = Item.sellPrice(0, 0, 5, 0);
        }
    }
}
