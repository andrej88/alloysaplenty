﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class PeridotStaff : ModItem
    {

        public override void SetDefaults()
        {
            item.name = "Peridot Staff";
            item.useSound = 43;
            item.useStyle = 5;
            item.width = 40;
            item.height = 40;
            item.magic = true;
            item.noMelee = true;
            Item.staff[item.type] = true;
            item.rare = 1;

            item.mana = 6;
            item.damage = 15;
            item.shoot = mod.ProjectileType("PeridotBolt");
            item.useAnimation = 32;
            item.useTime = 32;
            item.shootSpeed = 6.5f;
            item.knockBack = 3.5f;
            item.autoReuse = true;
            item.value = Item.sellPrice(0, 0, 25, 0);
        }
    }
}
