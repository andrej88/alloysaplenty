﻿using Microsoft.Xna.Framework;
using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class MagentaPhaseblade : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Magenta Phaseblade";
            item.useStyle = 1;
            item.useAnimation = 25;
            item.knockBack = 3f;
            item.width = 40;
            item.height = 40;
            item.damage = 21;
            item.scale = 1f;
            item.useSound = 15;
            item.rare = 1;
            item.value = Item.sellPrice(0, 0, 54, 0);
            item.melee = true;
        }

        public override Color? GetAlpha(Color lightColor)
        {
            return Color.White;
        }

        public override void MeleeEffects(Player player, Microsoft.Xna.Framework.Rectangle hitbox)
        {
            Lighting.AddLight((int)player.position.X / 16, (int)player.position.Y / 16, 0.9137f * 0.5f, 0.4863f * 0.5f, 0.6f * 0.5f);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.MeteoriteBar, 15);
            recipe.AddIngredient(mod, "Garnet", 10);
            recipe.AddTile(TileID.Anvils);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
