﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class BronzeBow : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Bronze Bow";
            item.useStyle = 5;
            item.useAnimation = 28;
            item.useTime = 28;
            item.width = 16;
            item.height = 32;
            item.shoot = 1;
            item.useAmmo = ProjectileID.WoodenArrowFriendly;
            item.useSound = 5;
            item.damage = 8;
            item.shootSpeed = 6.6f;
            item.noMelee = true;
            item.value = Item.sellPrice(0, 0, 0, 90);
            item.ranged = true;
            item.rare = 0;
            item.autoReuse = false;
            
        }
    }
}