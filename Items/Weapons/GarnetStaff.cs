﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class GarnetStaff : ModItem
    {

        private int projectileSpread = 18;

        public override void SetDefaults()
        {
            item.name = "Garnet Staff";
            item.useSound = 43;
            item.useStyle = 5;
            item.width = 40;
            item.height = 40;
            item.magic = true;
            item.noMelee = true;
            Item.staff[item.type] = true;
            item.rare = 1;

            item.mana = 3;
            item.damage = 5;
            item.shoot = mod.ProjectileType("GarnetBolt");
            item.useAnimation = 15;
            item.useTime = 15;
            item.shootSpeed = 9f;
            item.knockBack = 3.5f;
            item.autoReuse = true;
            item.value = Item.sellPrice(0, 0, 15, 0);
        }

        public override bool Shoot(Player player, ref Microsoft.Xna.Framework.Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)
        {
            float newAngleOffset = (float)((Main.rand.Next(projectileSpread) - (projectileSpread / 2)) * (3.14159 / 180));
            float currentAngle;
            if (speedX > 0)
            {
                currentAngle = (float)Math.Atan(speedY / speedX);
            }
            else
            {
                currentAngle = (float)Math.Atan(speedY / speedX) + 3.14159f;
            }
            float newAngle = currentAngle + newAngleOffset;
            speedX = item.shootSpeed * (float)Math.Cos((double)newAngle);
            speedY = item.shootSpeed * (float)Math.Sin((double)newAngle);
            return true;
        }
    }
}
