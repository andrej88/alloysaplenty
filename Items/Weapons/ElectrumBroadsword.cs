﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class ElectrumBroadsword : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Electrum Broadsword";  //
            item.useStyle = 1;
            item.useTurn = false;   //
            item.useAnimation = 18; //
            item.useTime = 18;      //
            item.width = 32;
            item.height = 32;
            item.damage = 5;       //
            item.knockBack = 0f;    //
            item.useSound = 1;
            item.scale = 1f;
            item.melee = true;

            item.value = Item.sellPrice(0, 0, 23, 0);
            item.rare = 0;
            item.autoReuse = true;  //
        }
    }
}