﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class HepatizonBow : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Hepatizon Bow"; //
            item.useStyle = 5;
            item.useAnimation = 26;     //
            item.useTime = 26;          //
            item.width = 16;
            item.height = 32;
            item.shoot = 1;
            item.useAmmo = ProjectileID.WoodenArrowFriendly;
            item.useSound = 5;
            item.damage = 10;            //
            item.shootSpeed = 6.6f;     //
            item.noMelee = true;
            item.value = Item.sellPrice(0, 0, 5, 50);
            item.ranged = true;
            item.rare = 0;              //
            item.autoReuse = false;      //
        }
    }
}