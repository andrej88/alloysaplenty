﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class BronzeBroadsword : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Bronze Broadsword";
            item.useStyle = 1;
            item.useTurn = false;
            item.useAnimation = 21;
            item.useTime = 21;
            item.width = 32;
            item.height = 32;
            item.damage = 10;
            item.knockBack = 4f;
            item.useSound = 1;
            item.scale = 1f;
            item.melee = true;
            item.value = Item.sellPrice(0, 0, 1, 15);

            item.rare = 0;
            item.autoReuse = false;
        }
    }
}