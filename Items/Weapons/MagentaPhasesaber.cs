﻿using Microsoft.Xna.Framework;
using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Weapons
{
    public class MagentaPhasesaber : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Magenta Phasesaber";
            item.useStyle = 1;
            item.useAnimation = 25;
            item.knockBack = 3f;
            item.width = 40;
            item.height = 40;
            item.damage = 41;
            item.scale = 1f;
            item.useSound = 15;
            item.value = Item.sellPrice(0, 0, 54, 0);
            item.melee = true;
            item.scale = 1.15f;
            item.autoReuse = true;
            item.useTurn = true;
            item.rare = 4;
        }

        public override Color? GetAlpha(Color lightColor)
        {
            return Color.White;
        }

        public override void MeleeEffects(Player player, Microsoft.Xna.Framework.Rectangle hitbox)
        {
            Lighting.AddLight((int)player.position.X / 16, (int)player.position.Y / 16, 0.9137f * 0.5f, 0.4863f * 0.5f, 0.6f * 0.5f);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(mod, "MagentaPhaseblade");
            recipe.AddIngredient(ItemID.CrystalShard, 50);
            recipe.AddTile(TileID.MythrilAnvil);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
