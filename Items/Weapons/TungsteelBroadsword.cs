﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class TungsteelBroadsword : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Tungsteel Broadsword";  //
            item.useStyle = 1;
            item.useTurn = false;   //
            item.useAnimation = 30; //
            item.useTime = 30;      //
            item.width = 32;
            item.height = 32;
            item.damage = 18;       //
            item.knockBack = 6f;    //
            item.useSound = 1;
            item.scale = 1.15f;
            item.melee = true;

            item.value = Item.sellPrice(0, 0, 8, 50);
            item.rare = 0;
            item.autoReuse = false;  //
        }
    }
}