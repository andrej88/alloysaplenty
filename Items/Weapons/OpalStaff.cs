﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace AlloysAplenty.Items.Weapons
{
    public class OpalStaff : ModItem
    {

        public override void SetDefaults()
        {
            item.name = "Opal Staff";
            item.useSound = 43;
            item.useStyle = 5;
            item.width = 40;
            item.height = 40;
            item.magic = true;
            item.noMelee = true;
            Item.staff[item.type] = true;
            item.rare = 1;

            item.mana = 11;
            item.damage = 28;
            item.shoot = mod.ProjectileType("OpalBolt");
            item.useAnimation = 38;
            item.useTime = 38;
            item.shootSpeed = 4f;
            item.knockBack = 3.5f;
            item.autoReuse = true;
            item.value = Item.sellPrice(0, 0, 50, 0);
        }
    }
}
