﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Placeable
{
    public class Quartz : ModItem
    {

        public override void SetDefaults()
        {
            item.name = "Quartz";
            item.width = 10;
            item.height = 14;
            item.maxStack = 999;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.useStyle = 1;
            //item.consumable = true;
            item.alpha = 50;

            item.value = Item.sellPrice(0, 0, 3, 75);
            //item.createTile = mod.TileType("QuartzTile");
        }
    }
}
