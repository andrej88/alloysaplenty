﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using Microsoft.Xna.Framework;

namespace AlloysAplenty.Items.Placeable
{
    public class BeigeTorch : ModItem
    {
        
        public override void SetDefaults()
        {
            item.flame = true;
            item.noWet = true;
            item.name = "Beige Torch";
            item.useStyle = 1;
            item.useTurn = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.holdStyle = 1;
            item.autoReuse = true;
            item.maxStack = 99;
            item.consumable = true;
            item.createTile = mod.TileType("BeigeTorchTile");
            item.placeStyle = 2;
            item.width = 10;
            item.height = 12;
            item.value = 200;
        }

        public override void HoldItem(Player player)
        {
            Lighting.AddLight(player.position, 0.7255f, 0.7020f, 0.6667f);
        }

        //public override void AddRecipes()
        //{
        //    ModRecipe recipe = new ModRecipe(mod);
        //    recipe.AddIngredient(ItemID.Torch, 3);
        //    recipe.AddIngredient(mod, "Quartz", 1);
        //    recipe.SetResult(this, 3);
        //    recipe.AddRecipe();
        //}

    }
}
