﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Placeable
{
    public class RoseGoldBar : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Rose Gold Bar";
            item.width = 12;
            item.height = 12;
            item.maxStack = 99;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.useStyle = 1;
            item.consumable = true;
            item.createTile = mod.TileType("RoseGoldBarTile");
            item.value = Item.sellPrice(0, 0, 10, 0);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.CopperBar);
            recipe.AddIngredient(ItemID.GoldBar);
            recipe.AddTile(TileID.Furnaces);
            recipe.SetResult(this, 2);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 30);
            recipe.AddIngredient(ItemID.Silk, 20);
            recipe.AddTile(TileID.Anvils);
            recipe.SetResult(ItemID.Throne);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 5);
            recipe.AddIngredient(ItemID.Torch, 3);
            recipe.AddTile(TileID.WorkBenches);
            recipe.SetResult(ItemID.Candelabra);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 4);
            recipe.AddIngredient(ItemID.Torch, 4);
            recipe.AddIngredient(ItemID.Chain, 1);
            recipe.AddTile(TileID.Anvils);
            recipe.SetResult(ItemID.GoldChandelier);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 1);
            recipe.AddIngredient(ItemID.Torch, 1);
            recipe.AddTile(TileID.WorkBenches);
            recipe.SetResult(ItemID.Candle);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 5);
            recipe.AddIngredient(ItemID.Ruby, 1);
            recipe.AddTile(TileID.Anvils);
            recipe.SetResult(ItemID.GoldCrown);
            recipe.AddRecipe();

            recipe = new ModRecipe(mod);
            recipe.AddCraftGroup(mod, "AnyGoldenBar", 10);
            recipe.AddIngredient(ItemID.Chain, 1);
            recipe.AddTile(TileID.Tables);
            recipe.AddTile(TileID.Chairs);
            recipe.SetResult(ItemID.GoldWatch);
            recipe.AddRecipe();
        }
    }
}
