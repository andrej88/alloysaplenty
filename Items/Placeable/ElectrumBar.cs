﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Placeable
{
    public class ElectrumBar : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Electrum Bar";
            item.width = 12;
            item.height = 12;
            item.maxStack = 99;
            item.useTurn = true;
            item.autoReuse = true;
            item.useAnimation = 15;
            item.useTime = 10;
            item.useStyle = 1;
            item.consumable = true;
            item.createTile = mod.TileType("ElectrumBarTile");
            item.value = Item.sellPrice(0, 0, 9, 0);
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.SilverBar);
            recipe.AddIngredient(ItemID.GoldBar);
            recipe.AddTile(TileID.Furnaces);
            recipe.SetResult(this, 2);
            recipe.AddRecipe();
        }
    }
}
