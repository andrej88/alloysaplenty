﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class BronzeHelmet : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Head);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Bronze Helmet";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 3, 0);
            item.defense = 2;
        }

        public override void UpdateEquip(Player player)
        {
            // Effects go here
        }

        public override bool IsArmorSet(Item head, Item body, Item legs)
        {
            return body.type == mod.ItemType("BronzeChainmail") && legs.type == mod.ItemType("BronzeGreaves");
        }

        public override void UpdateArmorSet(Player player)
        {
            // Set Bonii go here
        }
    }
}