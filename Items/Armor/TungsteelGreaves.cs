﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class TungsteelGreaves : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Legs);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Tungsteel Greaves";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 7, 0);
            item.defense = 5;
            item.toolTip = "5% reduced movement speed";
        }

        public override void UpdateEquip(Player player)
        {
            player.moveSpeed -= 0.05f;
        }
    }
}