﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class TungsteelHelmet : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Head);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Tungsteel Helmet";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 12, 0);
            item.defense = 5;
            item.toolTip = "5% reduced movement speed";
        }

        public override void UpdateEquip(Player player)
        {
            player.moveSpeed -= 0.05f;
        }

        public override bool IsArmorSet(Item head, Item body, Item legs)
        {
            return body.type == mod.ItemType("TungsteelChainmail") && legs.type == mod.ItemType("TungsteelGreaves");
        }

        public override void UpdateArmorSet(Player player)
        {
            player.setBonus = "3 defense\n5% reduced movement speed";
            player.statDefense += 3;
            player.moveSpeed -= 0.05f;
        }
    }
}