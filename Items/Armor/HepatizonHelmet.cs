﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class HepatizonHelmet : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Head);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Hepatizon Helmet";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 25, 0);
            item.defense = 3;
            item.toolTip = "20% increased throwing velocity";
        }

        public override void UpdateEquip(Player player)
        {
            player.thrownVelocity *= 1.2f;
        }

        public override bool IsArmorSet(Item head, Item body, Item legs)
        {
            return body.type == mod.ItemType("HepatizonChainmail") && legs.type == mod.ItemType("HepatizonGreaves");
        }

        public override void UpdateArmorSet(Player player)
        {
            //foreach(Item i in player.inventory)
            //{
            //    if (i.name.ToLower().Contains("hepatizon"))
            //    {
            //        if (i.damage != 0)
            //            i.damage = (int)(i.damage * 1.20f);     // damage increased by 20% for hepatizon weapons/tools
                    
            //        if (i.pick > 0 || i.axe > 0 || i.hammer > 0 || i.damage > 0)
            //        {
            //            i.useTime = (int)(i.useTime * 0.8f);
            //            i.useAnimation = (int)(i.useAnimation * 0.8f);
            //        }                                           // speed increased by 20% for hepatizon weapons/tools

            //        if (i.shootSpeed > 0)
            //            i.shootSpeed *= 1.2f;                   // make bows shoot faster arrows
            //    }
            //    else if (i.name.ToLower().Contains("peridot staff"))
            //    {
            //        i.damage = (int)(i.damage * 1.2f);
            //        i.mana = (int)(i.mana * 0.75f);
            //    }
            //}
            //player.setBonus = "Hepatizon tools and weapons are more effective\nDon't ask how, haven't figured that one out yet...";

            player.thrownCost33 = true;
            player.setBonus = "33% chance not to consume thrown item";
        }
    }
}