﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class BronzeChainmail : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Body);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Bronze Chainmail";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 2, 50);
            item.defense = 3;
        }

        public override void UpdateEquip(Player player)
        {
            // Effects go here
        }
    }
}