﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class HepatizonChainmail : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Body);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Hepatizon Chainmail";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 15, 0);
            item.defense = 3;
            item.toolTip = "30% increased throwing damage";
        }

        public override void UpdateEquip(Player player)
        {
            player.thrownDamage *= 1.3f;
        }
    }
}