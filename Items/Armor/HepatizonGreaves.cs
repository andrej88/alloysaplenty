﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class HepatizonGreaves : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Legs);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Hepatizon Greaves";
            item.width = 18;
            item.height = 18;
            item.value = Item.sellPrice(0, 0, 10, 0);
            item.defense = 3;
            item.toolTip = "5% increased throwing critical strike chance";
        }

        public override void UpdateEquip(Player player)
        {
            player.thrownCrit += 5;
        }
    }
}