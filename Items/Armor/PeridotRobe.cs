﻿using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Armor
{
    public class PeridotRobe : ModItem
    {
        public override bool Autoload(ref string name, ref string texture, IList<EquipType> equips)
        {
            equips.Add(EquipType.Body);
            return true;
        }

        public override void SetDefaults()
        {
            item.name = "Peridot Robe";
            item.width = 18;
            item.height = 14;
            item.defense = 2;
            item.value = Item.sellPrice(0, 1, 75, 0);
            item.toolTip = "Increases maximum mana by 60";
            item.toolTip2 = "Reduces mana usage by 10%";
        }

        public override void UpdateEquip(Player player)
        {
            player.manaCost -= 0.10f;
            player.statManaMax2 += 60;
        }

        public override bool IsArmorSet(Item head, Item body, Item legs)
        {
            return head.type == ItemID.WizardHat || head.type == ItemID.MagicHat;
        }

        public override void UpdateArmorSet(Player player)
        {
            if (player.head == 14)          // Wizard Hat
            {
                player.setBonus = "10% increased magic critical strike chance";
                player.magicCrit += 10;
            }
            else if (player.head == 159)    // Magic Hat
            {
                player.setBonus = "Increases maximum mana by 60";
                player.statManaMax2 += 60;
            }
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Robe);
            recipe.AddIngredient(mod, "Peridot", 10);
            recipe.AddTile(TileID.Loom);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
