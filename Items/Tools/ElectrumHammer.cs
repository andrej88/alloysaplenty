﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class ElectrumHammer : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Electrum Hammer";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 1.0f;
            item.value = Item.sellPrice(0, 0, 20, 0);
            item.useTime = 16;           //
            item.useAnimation = item.useTime + 8;     //
            item.hammer = 35;             // Hammer Power
            item.damage = 5;            //
            item.tileBoost = -2;         // range
        }
    }
}
