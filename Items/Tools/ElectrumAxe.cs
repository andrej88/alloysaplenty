﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class ElectrumAxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Electrum Axe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 28;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 0f;
            item.useTime = 11;           //
            item.useAnimation = item.useTime + 8;     //
            item.axe = 6;             // Axe Power
            item.damage = 4;            //
            item.tileBoost = -2;         // range
            item.value = Item.sellPrice(0, 0, 20, 0);
        }
    }
}
