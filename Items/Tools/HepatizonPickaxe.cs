﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class HepatizonPickaxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Hepatizon Pickaxe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2f;
            item.useTime = 15;          //
            item.useAnimation = 15;     //
            item.pick = 45;             // Pickaxe Power
            item.damage = 6;            //
            item.tileBoost = 0;         // range
            item.value = Item.sellPrice(0, 0, 9, 0);
        }
    }
}
