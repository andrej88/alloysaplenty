﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class BronzeHammer : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Bronze Hammer";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2.5f;
            item.value = Item.sellPrice(0, 0, 1, 0);
            item.useTime = 20;          //
            item.useAnimation = item.useTime + 8;     //
            item.hammer = 40;             // Hammer Power
            item.damage = 5;            //
            item.tileBoost = 0;         // range
        }
    }
}
