﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class ElectrumPickaxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Electrum Pickaxe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 0f;
            item.useTime = 11;           //
            item.useAnimation = 11;     //
            item.pick = 35;             // Pickaxe Power
            item.damage = 5;            //
            item.tileBoost = -2;         // range
            item.value = Item.sellPrice(0, 0, 15, 0);
        }
    }
}
