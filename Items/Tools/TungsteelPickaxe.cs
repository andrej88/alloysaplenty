﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class TungsteelPickaxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Tungsteel Pickaxe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2f;
            item.scale = 1.15f;
            item.useTime = 20;          //
            item.useAnimation = 20;     //
            item.pick = 60;             // Pickaxe Power
            item.damage = 7;            //
            item.tileBoost = 0;         // range
            item.value = Item.sellPrice(0, 0, 12, 0);
        }
    }
}
