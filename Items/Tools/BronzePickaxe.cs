﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class BronzePickaxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Bronze Pickaxe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2f;
            item.useTime = 17;          //
            item.useAnimation = 17;     //
            item.pick = 40;             // Pickaxe Power
            item.damage = 5;            //
            item.tileBoost = 0;         // range
            item.value = Item.sellPrice(0, 0, 1, 0);
        }
    }
}
