﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class TungsteelHammer : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Tungsteel Hammer";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 32;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 6.5f;
            item.value = Item.sellPrice(0, 0, 7, 0);
            item.scale = 1.15f;
            item.useTime = 23;          //
            item.useAnimation = item.useTime + 8;     //
            item.hammer = 60;             // Hammer Power
            item.damage = 7;            //
            item.tileBoost = 0;         // range
        }
    }
}
