﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class HepatizonAxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Hepatizon Axe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 28;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2f;
            item.useTime = 19;          //
            item.useAnimation = item.useTime + 8;     //
            item.axe = 9;             // Axe Power divided by 5
            item.damage = 5;            //
            item.tileBoost = 0;         // range
            item.value = Item.sellPrice(0, 0, 7, 0);
        }
    }
}
