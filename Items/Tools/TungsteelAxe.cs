﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace AlloysAplenty.Items.Tools
{
    public class TungsteelAxe : ModItem
    {
        public override void SetDefaults()
        {
            item.name = "Tungsteel Axe";
            item.useStyle = 1;
            item.useTurn = true;
            item.autoReuse = true;
            item.width = 32;
            item.height = 28;
            item.useSound = 1;
            item.melee = true;
            item.knockBack = 2f;
            item.scale = 1.15f;
            item.useTime = 22;          //
            item.useAnimation = item.useTime + 8;     //
            item.axe = 12;             // Axe Power
            item.damage = 6;            //
            item.tileBoost = 0;         // range
            item.value = Item.sellPrice(0, 0, 7, 0);
        }
    }
}
